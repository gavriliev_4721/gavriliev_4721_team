﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using e_business_170316.project1;

namespace e_business_170316
{
    class Program
    {
        static void Main(string[] args)
        {
            patp patp1 = new patp();
            patp1.nameofpatp = "ПАТП № 8";
            patp1.adress = "ул. Нижегородская, 272";

            reis reis1 = new reis();
            reis1.destination = "Каргат";
            reis1.dateofreis = new DateTime(2016, 12, 21, 16, 20, 00);
            reis1.price = 300;
            
            bus bus1 = new bus();
            bus1.gov_number = "К556КВ";
            bus1.mark = "ПАЗ";
            bus1.model = "Вектор";
            bus1.seatnumber = 5;

            reis1.bus_appoint(bus1);

            passenger[] pass1 = new passenger[3];
            pass1[0] = new passenger();
            pass1[0].fullname = "Сидорова Наталья Станиславовна";
            pass1[0].birthdate = new DateTime(1972,01,25);
            pass1[1] = new passenger();
            pass1[1].fullname = "Кольцов Валентин Викторович";
            pass1[1].birthdate = new DateTime(1978, 02, 13);
            pass1[2] = new passenger();
            pass1[2].fullname = "Стрелков Вадим Олегович";
            pass1[2].birthdate = new DateTime(1969, 08, 21);

            reis1.add_passenger(pass1);
            reis1.passengershow(reis1);
                     
            driver driver1 = new driver();
            driver1.fullname = "Голиков Олег Петрович";
            driver1.birthdate = new DateTime(1966,09,15);                    

            Console.ReadLine();
        }

      
       
    }
}
