﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace e_business_170316.project1
{
    class reis
    {
        private string _destination = "";
        private DateTime _dateofreis = new DateTime(1991, 01, 01,00,00,00);
        private double _price = 0;
        private passenger[] _numberofpass = new passenger[0];

        public string destination 
        {
            get 
            {
                return _destination;            
            }
            set 
            {
                if (value != null) 
                {
                    _destination = value;
                }
            }        
        }

        public DateTime dateofreis 
        {
            get 
            {
                return _dateofreis;
            }
            set 
            {
                if (value != null) 
                {
                    _dateofreis = value;
                }            
            }        
        }

        public double price
        {
            get 
            {
                return _price;
            }
            set
            {
                if (value != 0) 
                {
                    _price = value;
                }
            }
        }

        public passenger[] numberofpass
        {
            get
            {
                return _numberofpass;
            }
            set
            {
                _numberofpass = value;
            }
        }

        public virtual void bus_appoint(bus bus1) 
        {
            numberofpass = new passenger[bus1.seatnumber];
        }

        public virtual void add_passenger(passenger[] addpass)
        {
            passenger[] p = new passenger[addpass.Length];
            for (int i = 0; i < addpass.Length; i++)
            {
                p[i] = addpass[i];
            }
            for (int i = 0; i < numberofpass.Length; i++)
            {
                if (i > p.Length - 1)
                {
                    break;
                }
                if (numberofpass[i] == null)
                {
                    numberofpass[i] = p[i];
                }
                
            }
                        
            if (addpass.Length == 1)
            {
                Console.WriteLine("Был добавлен 1 пассажир" + "\n");
            }
            if (addpass.Length>=2 && addpass.Length<=4)
            {
                Console.WriteLine("Было добавлено " + addpass.Length + " пассажира" + "\n");
            }
            if (addpass.Length>=5)
            {
                Console.WriteLine("Было добавлено " + addpass.Length + " пассажиров" + "\n");
            }

           }

        public virtual void passengershow(reis reisinput)
        {
            int x = 0;
            Console.WriteLine("Информация о рейсе:" + "\n" + "Пункт назначения: " + reisinput.destination + "\n" + "Дата рейса :" + reisinput.dateofreis + "\n" + "Цена билета: " + reisinput.price + "\n" + "Количество мест:" + reisinput.numberofpass.Length);
            for (int i = 0; i < reisinput._numberofpass.Length; i++)
            {
                if (reisinput._numberofpass[i] != null )
                {
                    x= x + i;
                }
            }
            for (int i = 0; i < x; i++)
            {
                int v = i + 1;
                Console.WriteLine("Пассажир №: " + v + "\n" + "Ф.И.О: " + reisinput._numberofpass[i].fullname + "\n" + "Дата рождения: " + reisinput._numberofpass[i].birthdate);
            }
        }

       }
}

