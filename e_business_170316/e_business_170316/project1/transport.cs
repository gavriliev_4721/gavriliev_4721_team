﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace e_business_170316.project1
{
    class transport
    {
        private string _gov_number = "";
        private string _mark = "";
        private string _model = "";
        private int _seatnumber = 0;

        public string gov_number 
        {
            get 
            {
                return _gov_number;
            }
            set 
            {
                if (value != null) 
                {
                    _gov_number = value;
                }            
            }        
        }

        public string mark 
        {
            get 
            {
                return _mark;
            }
            set 
            {
                 if (value != null)
                 {
                     _mark = value; 
                 }
            }
        }

        public string model
        {
            get 
            {
                return _model;
            }
            set 
            {
                if (value != null) 
                {
                    _model = value;
                }
            }
        }

        public int seatnumber 
        {
            get 
            {
                return _seatnumber;
            }
            set 
            {
                if (value != 0) 
                {
                    _seatnumber = value;                
                }            
            }         
        }
    }
}
