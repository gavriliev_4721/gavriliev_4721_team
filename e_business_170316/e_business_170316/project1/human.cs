﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace e_business_170316.project1
{
    class human
    {
        private string _fullname = "";
        private DateTime _birthdate = new DateTime(1991, 01, 01);

        public string fullname
        {
            get 
            {
                return _fullname;
            }
            set 
            {
                if (value != null) 
                {
                    _fullname = value;
                }
            
            }
        }

        public DateTime birthdate
        {
            get 
            {
                return _birthdate;
            }
            set 
            {
                if (value != null) 
                {
                    _birthdate = value;              
                }            
            }
        }

    }
}
