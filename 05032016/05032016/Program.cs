﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _05032016.projectlibrary;


namespace _05032016
{
    class Program
    {
        static void Main(string[] args)
        {
           electroguitar strat = new electroguitar();
           strat.manufacturer = "Fender";
           strat.numberofstrings = 6;
           strat.name = "Stratocaster";
           strat.neck = "Клен";
           strat.body = "Ольха";
           strat.pickup = "EMG 80/EMG 60";
           strat.bridge = "Тремоло";
           Console.WriteLine(strat.ToString());
           Console.WriteLine("-------------------");

           acousticguitar cruser = new acousticguitar();
           cruser.manufacturer = "Flight";
           cruser.numberofstrings = 6;
           cruser.name = "Cruser";
           cruser.body = "Катальпа";
           cruser.backbody = "Ель";
           cruser.neck = "Клен";
           Console.WriteLine(cruser.ToString());
           Console.WriteLine("-------------------");

           electroacousticguitar crafter = new electroacousticguitar();
           crafter.manufacturer = "Crafter";
           crafter.numberofstrings = 12;
           crafter.name = "SM-Rose";
           crafter.body = "Ель Энгельмана";
           crafter.backbody = "Палисандр";
           crafter.neck = "Красное дерево";
           crafter.pickup = "LRT-DX";
           crafter.tuner = true;
           Console.WriteLine(crafter.ToString());
           Console.WriteLine("-------------------");

           ukulele hohner = new ukulele();
           hohner.manufacturer = "Hohner";
           hohner.name = "Lanikai LUTU21C";
           hohner.body = "Махагон";
           hohner.backbody = "Махагон";
           hohner.neck = "Окуме";
           hohner.size = "Баритон";
           Console.WriteLine(hohner.ToString());

           Console.ReadLine();  


        }
        
    }
}
