﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05032016.projectlibrary
{
    class ukulele:guitar
    {
        private string _sizeofukulele;
        private string _backbody;

        public string backbody
        {
            get
            {
                return _backbody;
            }
            set
            {
                if (value != null)
                    _backbody = value;
            }
        }

        public string size
        {
            get 
            {
                return _sizeofukulele;
            }
            set
            {
                if (value != null)
                    _sizeofukulele = value;
            }
        }

        public string typeofguitar
        {
            get
            {
                return "Укулеле";
            }
        }

        public int numberofstrings
        {
            get
            {
                return 4;
            }
           

        }

        public override string ToString()
        {
            return string.Format("Тип гитары:" + typeofguitar + "\n" + "Производитель:" + manufacturer + "\n" + "Название модели:" + name + "\n" + "Количество струн:" + numberofstrings + "\n" + "Дека:" + body + "\n" + "Задняя дека:" + backbody + "\n" + "Размер укулеле:" + size );
        }

    }
}
