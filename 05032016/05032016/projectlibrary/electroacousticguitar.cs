﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05032016.projectlibrary
{
    class electroacousticguitar:acousticguitar
    {
        private string _pickup;
        private bool _tuner;

     
       public string typeofguitar
        {
            get
            {
                return "Электроакустика";
            }
        }
        public string pickup
        {
            get 
            {
                return _pickup;
            }
            set
            {
                if (value != null)
                    _pickup = value;
            }
        }
        
        public bool tuner
        {
            get 
            {
                return _tuner;
            }
            set
            {
                if (value != null)
                    _tuner = value;
            }
        }

        public override string ToString()
        {
            return string.Format("Тип гитары:" + typeofguitar + "\n" + "Производитель:" + manufacturer + "\n" + "Название модели:" + name + "\n" + "Количество струн:" + numberofstrings + "\n" + "Дека:" + body + "\n" + "Задняя дека:" + backbody + "\n" + "Звукосниматель:" + pickup + "\n" + "Наличие тюнера:" + tuner);
        }

        }
}
