﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05032016.projectlibrary
{
    class guitar
    {
        private string _manufacturer = "";
        private int _numberofstings = 0;
        private string _body = "";
        private string _neck = "";
        private string _name = "";
        private string _typeofguitar = "";
        //public guitar()
        //{
        //    _manufacturer = "Fender" ;
        //    _numberofstings = 6;
        //    _body = "alder";
        //    _neck = "maple";
        //}

        //public guitar(string manufacturer, int numberofstrings, string body, string neck)
        //{
        //    _manufacturer = manufacturer;
        //    _numberofstings = numberofstrings;
        //    _body = body;
        //    _neck = neck;
        //}


         public string typeofguitar 
        { 
         get
            {
                return _typeofguitar;
            }
            set
            {
                if (value != null)
                    _typeofguitar = value;
            }
        }

        public string name
        {
            get 
            {
                return _name;
            }
            set 
            {
                if (value != null)
                    _name = value;
            }
        }
        public string manufacturer
        {
            get
            {
                return _manufacturer;
            }
            set
            {
                if (value != null)             
                _manufacturer = value;
                
            }
        }

        public int numberofstrings
        {
            get
            {
                return _numberofstings;
            }
            set
            {
                if (value != 0)    
                _numberofstings = value;
            }
        }

        public string body
        {
            get 
            {
                return _body;
            }
            set
            {
                if (value != null)    
                _body = value;
            }
        }

        public string neck
        {
            get 
            {
                return _neck;
            }
            set 
            {
                if (value != null)    
                _neck = value;
            }
        }

     

    }
}
